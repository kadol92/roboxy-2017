package pl.codedol.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import pl.codedol.model.User;
import pl.codedol.repository.UserRepository;
import pl.codedol.service.UserService;

import javax.validation.Valid;
import java.util.List;

@Controller
public class RegisterController {


    private UserService userService;

    @Autowired
    public RegisterController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/register")
    public String showRegisterPage(Model model) {
        model.addAttribute("user", new User(null, null, null));
        return "register";
    }


    //Receiving new user and validate it
    @PostMapping("/register")
    public String createNewUser(@Valid @ModelAttribute User user, BindingResult result) {
        if(result.hasErrors()){
            List<ObjectError> errors = result.getAllErrors();
            errors.forEach(err -> System.out.println(err.getDefaultMessage()));
            return "register";
        }
        userService.addWithDefaultRole(user);
        return "register_success";

    }
}
