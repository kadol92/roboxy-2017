package pl.codedol.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import pl.codedol.model.User;
import pl.codedol.model.UserRole;
import pl.codedol.service.UserService;

import java.util.HashSet;
import java.util.Set;

//Klasa implementująca własny system uwierzytelniania
//
public class CustomUserDetailsService implements UserDetailsService {

    private UserService userService;

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @Override
    public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
        //Get user from database
        User user = userService.findByLogin(login);

        if (user == null) {
            throw new UsernameNotFoundException("User not found");
        }

        //Create user for spring security with correct privileges
        org.springframework.security.core.userdetails.User userDetails =
                new org.springframework.security.core.userdetails
                        .User(user.getLogin(),
                        user.getPassword(),
                        convertAuthorities(user.getRoles()));

        return userDetails;
    }

    //Convert privileges from UserRole to GrantedAuthority
    private Set<GrantedAuthority> convertAuthorities(Set<UserRole> userRoles) {
        Set<GrantedAuthority> authorities = new HashSet<>();
        userRoles.forEach(ur -> authorities.add(new SimpleGrantedAuthority(ur.getRole())));
        return authorities;
    }

}
