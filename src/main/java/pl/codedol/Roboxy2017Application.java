package pl.codedol;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Roboxy2017Application {

	public static void main(String[] args) {
		SpringApplication.run(Roboxy2017Application.class, args);
	}
}
